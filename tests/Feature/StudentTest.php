<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\Schools\Entities\School;
use Modules\Students\Entities\Student;
class StudentTest extends TestCase
{
    use RefreshDatabase;
    

    public function test_get_students_data()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user);
        $response = $this->get('/api/students');
        
        $response->assertStatus(200);
    }

    public function test_get_create_student_data()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user);
        $school = School::factory()->create();
        $student = [
            "name" => "student1",
            "school_id" => $school->id
        ];
        $response = $this->post('/api/students', $student);
        
        $response->assertStatus(200);

        $response->assertSee('student1');
        
    }

    public function test_get_update_student_data()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user);

        $student = Student::factory()->create([
            "school_id" => School::factory()->create()->id
        ]);
        $student_edit = [
            "name" => "student1"
        ];
        $response = $this->put('/api/students/'.$student->id, $student_edit);
        
        $response->assertStatus(200);

        $response->assertSee('student1');
        
    }

    public function test_get_delete_student_data()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user);

        $student = Student::factory()->create([
            'name' => 'student2',
            "school_id" => School::factory()->create()->id
        ]);
        
        $response = $this->delete('/api/students/'.$student->id);
        
        $response->assertStatus(200);

        $response->assertDontSee('student2');
        
    }
}
