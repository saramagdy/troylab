<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\Schools\Entities\School;

class SchoolTest extends TestCase
{
    use RefreshDatabase;
    

    public function test_get_schools_data()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user);
        $response = $this->get('/api/schools');
        
        $response->assertStatus(200);
    }

    public function test_get_create_school_data()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user);
        $school = [
            "name" => "school2"
        ];
        $response = $this->post('/api/schools', $school);
        
        $response->assertStatus(200);

        $response->assertSee('school2');
        
    }
    public function test_get_create_with_error_school_data()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user);
        $school = [ ];
        $response = $this->post('/api/schools', $school);
        
        $response->assertStatus(422);

        
        
    }

    public function test_get_update_school_data()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user);

        $school = School::factory()->create();
        $school_edit = [
            "name" => "school2"
        ];
        $response = $this->put('/api/schools/'.$school->id, $school_edit);
        
        $response->assertStatus(200);

        $response->assertSee('school2');
        
    }

    public function test_get_delete_school_data()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user);

        $school = School::factory()->create(['name' => 'school2']);
        
        $response = $this->delete('/api/schools/'.$school->id);
        
        $response->assertStatus(200);

        $response->assertDontSee('school2');
        
    }
}
