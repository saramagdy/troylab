<?php
namespace Modules\Students\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Schools\Entities\School;
class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Students\Entities\Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'school_id' => School::factory()->create()->id,
        ];
    }
}

