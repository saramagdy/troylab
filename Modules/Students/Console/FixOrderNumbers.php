<?php

namespace Modules\Students\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Students\Services\StudentService;

class FixOrderNumbers extends Command
{
    private $studentService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'fix:orderNumber';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix order number for students.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(StudentService $studentService)
    {
        parent::__construct();
        $this->studentService = $studentService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return $this->studentService->fixOrderNumbers();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
