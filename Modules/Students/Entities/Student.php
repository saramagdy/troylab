<?php

namespace Modules\Students\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Schools\Entities\School;
class Student extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ["name", "order","school_id"];
    
    protected static function newFactory()
    {
        return \Modules\Students\Database\factories\StudentFactory::new();
    }

    public function school(){
        return $this->belongsTo(School::class, 'school_id');
    }
}
