<?php


namespace Modules\Students\Repositories;

use Modules\Base\Repositories\Classes\LaravelRepositoryClass;
use Modules\Students\Entities\Student;

class StudentRepository extends LaravelRepositoryClass
{


    public function __construct(Student $student)
    {
        $this->model = $student;
    }


    /* CMS Methods */
    public function paginate($per_page = 15, $conditions = null, $search_keys = null, $sort_key = 'id', $sort_order = 'asc', $lang = null)
    {
        $query = $this->filtering($search_keys);

        return parent::paginate($query, $per_page, $conditions, $sort_key, $sort_order);
    }

    public function all($conditions = [], $search_keys = null, $order_keys = null)
    {
        $query = $this->filtering($search_keys);
        if($order_keys){
            $this->ordering($query, $order_keys);
        }

        return $query->where($conditions)->get();
    }

    private function filtering($search_keys)
    {

        $query = $this->model;

        if ($search_keys) {
            $query = $query->where(function ($q) use ($search_keys) {
                $q->orWhere('id', 'LIKE', '%'.$search_keys.'%');
            });
        }

        return $query;
    }

    public function ordering($query , $order_keys)
    {
        foreach($order_keys as $key){
            $query->orderBy($key['name'], $key['value']);
        }
    }

   
}