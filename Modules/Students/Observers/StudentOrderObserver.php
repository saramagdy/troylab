<?php

namespace Modules\Students\Observers;


use Modules\Students\Entities\Student;

class StudentOrderObserver
{
    public function created(Student $object)
    {
        $last_student_in_this_school = Student::where('school_id',$object->school_id)->orderby('order','desc')->first();
        if($last_student_in_this_school){
            $student_order = $last_student_in_this_school->order +1;

        }else{
            $student_order = 1;
        }
        $object->update(['order'=>$student_order]);
    }
}