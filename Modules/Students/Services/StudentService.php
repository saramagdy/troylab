<?php


namespace Modules\Students\Services;


use Modules\Base\ResponseShape\ApiResponse;
use Modules\Base\Services\Classes\LaravelServiceClass;
use Modules\Students\Repositories\StudentRepository;
use Modules\Students\Transformers\StudentResource;
use Illuminate\Support\Facades\Auth;
use Mail;
class StudentService extends LaravelServiceClass
{
    private $studentRepository;

    public function __construct(StudentRepository $studentRepository)
    {
        $this->studentRepository = $studentRepository;
    }

    public function index()
    {
        if (request('is_pagination')) {
            list($schools, $pagination) = parent::paginate($this->studentRepository, false);
        } else {
            $schools = parent::list($this->studentRepository, false);

            $pagination = null;
        }
        $schools->load('school');
        return ApiResponse::format(200, StudentResource::collection($schools), null, $pagination);
    }

   
    public function store($request = null)
    {
        
        $request_data = $request->all();
       
       
        $school = $this->studentRepository->create(
            $request_data
        )
        ;
        return ApiResponse::format(200, StudentResource::make($school), 'Student created successfully');
    }

    public function show($id)
    {
        
        $school = $this->studentRepository->get($id, [], 'id');
        $school->load('school');
        return ApiResponse::format(200, StudentResource::make($school), 'Student data retrieved successfully');
    }

   
    public function update($id, $request = null)
    {
        
        $request_data = $request->all();
        
        
        $school = $this->studentRepository->update($id, $request_data, [], 'id');

        return ApiResponse::format(200, StudentResource::make($school), 'Student updated successfully');
    }

    
    public function delete($id)
    {
        $this->studentRepository->delete($id);
        return ApiResponse::format(200, [], 'Student deleted successfully');
    }


    public function fixOrderNumbers()
    {
        
        $students = $this->studentRepository->all([], null, [["name"=>"school_id","value"=>"asc"],['name'=>'id','value'=>'asc']]);
        $school_id=null;
        $last_order = 1;
        foreach($students as $student){
            if($school_id != $student->school_id){
                $school_id = $student->school_id;
                $student->order = 1;
                $last_order = 1;
            }
            else{
                $student->order = $last_order + 1;
                $last_order++;
            }
            $student->save();
        }
        
        Mail::send('mail', [], function($message) {
            $message->to('saramagdyahmed6@gmail.com')->subject
               ('Cron Job Success');
         });
    }
}