<?php

namespace Modules\Students\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Schools\Transformers\SchoolResource;

class StudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'school' => SchoolResource::make($this->whenLoaded('school')),
            'order' => $this->order
        ];
    }
}
