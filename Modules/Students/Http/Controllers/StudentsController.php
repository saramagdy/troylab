<?php

namespace Modules\Students\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Students\Services\StudentService;
use Modules\Base\Requests\PaginationRequest;
use Modules\Students\Http\Requests\StudentRequest;
class StudentsController extends Controller
{
    private $studentService;

    public function __construct(StudentService $studentService)
    {
        $this->studentService = $studentService;
    }
    /**
     * Display a listing of the resource.
     * @param PaginationRequest $request
     * @return JsonResponse
     */
    public function index(PaginationRequest $request)
    {
        return $this->studentService->index();
    }

    /**
     * Display a one resource.
     * @param DistrictRequest $request
     * @param $id
     * @return void
     */
    public function show($id)
    {
        return $this->studentService->show($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param DistrictRequest $request
     * @return JsonResponse
     */
    public function store(StudentRequest $request)
    {
        return $this->studentService->store($request);
    }

    /**
     * Update the specified resource in storage.
     * @param DistrictRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(StudentRequest $request, $id)
    {
        return $this->studentService->update($id, $request);
    }

    /**
     * Remove the specified resource from storage.
     * @param DistrictRequest $request
     * @return JsonResponse
     */
    public function destroy(StudentRequest $request)
    {
        return $this->studentService->delete($request->student);
    }
}
