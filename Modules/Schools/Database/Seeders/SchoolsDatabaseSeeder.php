<?php

namespace Modules\Schools\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Schools\Entities\School;
class SchoolsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        if(School::count() == 0){
            School::factory()->count(3)->create();
        }
       
       
    }
}
