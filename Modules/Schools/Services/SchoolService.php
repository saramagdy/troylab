<?php


namespace Modules\Schools\Services;


use Modules\Base\ResponseShape\ApiResponse;
use Modules\Base\Services\Classes\LaravelServiceClass;
use Modules\Schools\Repositories\SchoolRepository;
use Modules\Schools\Transformers\SchoolResource;
use Illuminate\Support\Facades\Auth;
class SchoolService extends LaravelServiceClass
{
    private $schoolRepository;

    public function __construct(SchoolRepository $schoolRepository)
    {
        $this->schoolRepository = $schoolRepository;
    }

    public function index()
    {
        if (request('is_pagination')) {
            list($schools, $pagination) = parent::paginate($this->schoolRepository, false);
        } else {
            $schools = parent::list($this->schoolRepository, false);

            $pagination = null;
        }

        return ApiResponse::format(200, SchoolResource::collection($schools), null, $pagination);
    }

    
    public function store($request = null)
    {
        
        $request_data = $request->all();
       
       
        $school = $this->schoolRepository->create(
            $request_data
        )
        ;
        return ApiResponse::format(200, SchoolResource::make($school), 'School created successfully');
    }

    public function show($id)
    {
        
        $school = $this->schoolRepository->get($id, [], 'id');
        return ApiResponse::format(200, SchoolResource::make($school), 'School data retrieved successfully');
    }

   
    public function update($id, $request = null)
    {
       
        $request_data = $request->all();
       
        $school = $this->schoolRepository->update($id, $request_data, [], 'id');

        return ApiResponse::format(200, SchoolResource::make($school), 'School updated successfully');
    }

   
    public function delete($id)
    {
        $this->schoolRepository->delete($id);
        return ApiResponse::format(200, [], 'School deleted successfully');
    }
}