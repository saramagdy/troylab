<?php

namespace Modules\Schools\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Schools\Services\SchoolService;
use Modules\Base\Requests\PaginationRequest;
use Modules\Schools\Http\Requests\SchoolRequest;
class SchoolsController extends Controller
{

    private $schoolService;

    public function __construct(SchoolService $schoolService)
    {
        $this->schoolService = $schoolService;
    }
    /**
     * Display a listing of the resource.
     * @param PaginationRequest $request
     * @return JsonResponse
     */
    public function index(PaginationRequest $request)
    {
        return $this->schoolService->index();
    }

    /**
     * Display a one resource.
     * @param DistrictRequest $request
     * @param $id
     * @return void
     */
    public function show($id)
    {
        return $this->schoolService->show($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param DistrictRequest $request
     * @return JsonResponse
     */
    public function store(SchoolRequest $request)
    {
        return $this->schoolService->store($request);
    }

    /**
     * Update the specified resource in storage.
     * @param DistrictRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(SchoolRequest $request, $id)
    {
        return $this->schoolService->update($id, $request);
    }

    /**
     * Remove the specified resource from storage.
     * @param DistrictRequest $request
     * @return JsonResponse
     */
    public function destroy(SchoolRequest $request)
    {
        return $this->schoolService->delete($request->school);
    }
}
