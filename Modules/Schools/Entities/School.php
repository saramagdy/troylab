<?php

namespace Modules\Schools\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
class School extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = ["name"];
    
    protected static function newFactory()
    {
        return \Modules\Schools\Database\factories\SchoolFactory::new();
    }
}
