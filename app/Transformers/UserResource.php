<?php

namespace App\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
/**
 * @property mixed admin
 * @property mixed user_type
 * @property mixed client
 * @property mixed name
 * @property mixed email
 * @property mixed status
 * @property mixed token_last_renew
 */
class UserResource extends JsonResource
{
    private $token;
    private $is_register;

    public function __construct($resource, $token = null, $is_register = false)
    {
        $this->token = $token;
        $this->is_register = $is_register;
        parent::__construct($resource);
    }


    /**
     * Transform the resource into an array.
     *
     * @param  Request
     * @return array
     */
    public function toArray($request)
    {
        
        $token = [];

        if ((Auth::id() == $this->id || $this->is_register == true) && isset($this->token)) {
            $token = [
                'token' => $this->token,
            ];
        }


        $default = [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
        ];

        return array_merge($token, $default);
    }
}
