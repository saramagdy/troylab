<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\UsersErrorsHelper;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerFacades();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }


    private function registerFacades(){
        $this->app->bind('UsersErrorsHelper', function () {
            return $this->app->make(UsersErrorsHelper::class);
        });
    }
}
