<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Modules\Base\ResponseShape\ApiResponse;
use Modules\Base\Services\Classes\LaravelServiceClass;


use App\Repositories\UserRepository;
use App\Transformers\UserResource;
use App\Facades\UsersErrorsHelper;
use \DB;
use Carbon\carbon;
class UserService extends LaravelServiceClass
{

    private $user_repo;


    public function __construct(UserRepository $user)
    {
        $this->user_repo = $user;
    }

    

    

  

    public function get()
    {
        $user = Auth::user();
     
        $user = $this->getUserResource($user);

        return ApiResponse::format(201, $user);
    }


     /**
     * Handles User login
     *
     * @return JsonResponse
     */
    public function login($request)
    {
        
        $loginStatus = $this->user_repo->get($request->email,[],'email');
        
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];

        
            $loginStatus = $this->user_repo->AuthAttempt($data);

            if ($loginStatus) {
                $user =  Auth::user();
                $tokenResult = $user->createToken(env('APP_NAME'));
    
                $user = $this->user_repo->update($user->id, ['token_last_renew' => Carbon::now()]);
    
                $token = $tokenResult->accessToken;
                $token->save();
    
                // Determine The Response Shape in login
                $auth = new UserResource($user, $tokenResult->plainTextToken, false);
    
                return ApiResponse::format(200, $auth, 'Login Successfully');
            } else {
                UsersErrorsHelper::unAuthenticated();
            }
        
       
    }


    /**
     * Handles Register
     *
     * @param $request
     * @return JsonResponse
     */
    public function register($request)
    {
        return DB::transaction(function () use ($request) {
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
            ];

 
                $data['password'] = bcrypt($data['password']);
                //Create User Record
                $user = $this->user_repo->create($data);
     
                // Save token Last Renew
                $tokenResult = $user->createToken(env('APP_NAME'));
                $user = $this->user_repo->update($user->id, ['token_last_renew' => Carbon::now()]);
            
                // Save New Token
                $token = $tokenResult->accessToken;
                $token->save();
    
                // Determine The Response Shape in register
                $auth = new UserResource($user, $tokenResult->plainTextToken, true);
    
                return ApiResponse::format(200, $auth, 'Register Successfully');
           
            
           
        });

    }
}