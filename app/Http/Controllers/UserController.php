<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;

class UserController extends Controller
{

    private $userService;

    public function __construct(UserService $user)
    {
        $this->userService = $user;
    }
    public function login(LoginRequest $request){
       return  $this->userService->login($request);

    }
    public function register(RegisterRequest $request){

        return  $this->userService->register($request);
    }
}
